#!/usr/bin/env python3
# Soubor:  fread.py
# Datum:   25.10.2023 13:27
# Autor:   Marek Nožka, nozka <@t> spseol <d.t> cz
############################################################################
from sys import stdin

with open("myfile.txt", "r") as f:
    znak = f.read(1)
    triznaky = f.read(3)
    radek = f.readline()

    f.seek(0, 1)
    konec = f.read()

    f.seek(0)
    zbytek = f.read()

print(znak, triznaky, radek, konec, zbytek)


neco = stdin.read()
print(neco)
