#!/usr/bin/env pypy
# Soubor:  myLMS.py
# Datum:   2023-10-25 12:59
# Autor:   Marek Nožka, nozka <@t> spseol <d.t> cz
##############################################################################
from sys import stdout, stderr
from os import path

stdout.write(path.dirname(__file__))
adr = path.dirname(__file__)

file_haldler = open(adr + "/myfile.txt", "w")
file_haldler = open("/dev/full", "w")

file_haldler.write("Halo Word!\n")
file_haldler.write("Ahoj miláčku!\n")

file_haldler.flush()
file_haldler.close()

with open("yourfile.txt", "a") as file_haldler:
    c = ord("a")
    while chr(c) != "z":
        file_haldler.write(chr(c))
        c += 1


stdout.write("KOnec\n")
